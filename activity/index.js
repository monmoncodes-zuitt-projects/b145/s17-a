console.log("Hello from JS")

/*
TASK 1: Create a function that will prompt the user for 2 numbers and perform different arithmetic operations based on the total of the two numbers:
a. If the total of the two numbers is less than 10, add the numbers
b. If the total of the two numbers is 10 - 20, subtract the numbers
c. If the total of the two numbers is 21 - 29 multiply the numbers
d. If the total of the two numbers is greater than or equal to 30, divide the numbers
e. Use an alert for the total of 10 or greater and a console */

function math(){
   let num1 = parseInt(prompt('Provide a number:'));
   let num2 = parseInt(prompt('Provide another number:'));
   if (num1 + num2 < 10) {
        console.log('The sum of the two numbers are: ' + (num1 + num2))
      
   }else if (num1 + num2 < 20) {
      alert('the difference of the two numbers are: ' + (num1 - num2));

   }else if (num1 + num2 < 30){
      alert('the product of the two numbers are: ' + (num1 * num2));
   }else {
      alert('the quotient of the two numbers are: ' + (num1 / num2));
   }
} 

/*2. Create a function that will prompt the user for their name and age and print out different alert messages based on the user input:
-> If the name OR age is blank/null, print the message are you a time traveler?
-> If the name AND age is not blank, print the message with the user’s name and age.*/


function userprofile(){
let uname = prompt('What is your name?');
   let uage = prompt('How old are you?');
   if ( uname === " " || uname === null || uage === " "  || uage === null) {
      alert('Are you an alien?');
   }else {
      alert('This is ' + uname + ' ' + uage + ' years of age');
   }
}




/*TASK 3: Create a function with switch case statement that will check if the user's age input is within a certain set of expected input:
- 18 - print the message You are now allowed to party.
- 21 - print the message You are now part of the adult society.
- 65 - print the message We thank you for your contribution to society.
- Any other value - print the message Are you sure you're not an alien?*/


function party(){
   let age = prompt('Provide an age:');
   let verifyage;

   switch(age) {
      case '18':
          verifyage = alert('You are now allowed to party.');
          break;
      case '21':
         verifyage = alert('You are now part of the adult society.');
         break;
      case '65':
         verifyage = alert('We thank you for your contribution to the society');
         break;
      default:
         verifyage = alert('Are you sure you\'re not an alien?'); 

   }
}


//create a function that will determine if the age is too old for preschool
function ageChecker(){
  //we are going to use a try-catch statement instead

  //get the input of the user.
  //the getElementById() will target a component within the document using its ID attribute
  //the "document" parameter describes the HTML document/file where the JS module is linked.
  // "value" => describes the value property of our elements
  let userInput = document.getElementById('age').value; 
  //alert(userInput); //checker
  //we will now target the element where we will display the output of this function.
  let message = document.getElementById('outputDisplay'); 
  console.log(typeof userInput); //string
  
  try {
   //lets make sure that the input inserted by the user is NOT equals to a blank string.
   //throw -> this statement examines the input and returns an error.
   if (userInput === '' ) throw 'the input is empty';
   //create a conditional statement that will check if the input in NOT a number 
   //in order to check if the value is NOT A NUMBER, We will use a isNaN()
   if (isNaN(userInput)) throw 'the input is Not a Number';
   if (userInput <= 0) throw 'Not a valid Input' 
   if (userInput <= 7) throw 'the input is good for preschool'; 
   if (userInput > 7) throw 'too old for preschool';
  } catch(err) {
     //the "err" is to define the error that will be thrown by the try section. so "err" is caught by the catch statement and a custom error message will be displayed.
     //how are we going to inject a value inside the html container?
     //=> using innerHTML property : 
     //syntax: element.innerHTML -> This will allow us to get/set the HTML markup contained within the element
     message.innerHTML = "Age Input: " + err; 
  } finally {
   //this statement here will be executed regardless of the result above.
   console.log('This is from the finally section');
   //lalabas both ung block of code indicated in the finally section including the outcome of the try statement.
  }

}

/*// Do not modify!
var fs = require("fs");
var input = fs.readFileSync(process.stdin.fd, "utf-8").trim().split("\n");
let numA = parseInt(input[0]);
let numB = parseInt(input[1]);

// Your code here!

let total = numA + numB;



// Testing mechanism -- do not modify!
console.log(total);



*/
/*// Do not modify!
var fs = require("fs");
var input = fs.readFileSync(process.stdin.fd, "utf-8").trim().split("\n");
let numA = parseInt(input[0]);
let numB = parseInt(input[1]);
let total = numA + numB;

// Your code here!

if (total >= 30) {
    console.log("The quotient of the two numbers is " + (numA / numB));
} else if (total >= 20) {
    console.log("The product of the two numbers is " + (numA * numB));
} else if (total >= 10) {
    console.log("The difference of the two numbers is " + (numA - numB));
} else {
    console.log("The sum of the two numbers is " + total);
}*/

/*// Do not modify!
var fs = require("fs");
var input = fs.readFileSync(process.stdin.fd, "utf-8").trim().split("\n");
let name = input[0];
let age = parseInt(input[1]);

// Your code here!

   if ( name === " " || name === null || age === " "  || age === null) {
      console.log('Are you a time traveler?');
   }else {
      console.log('Hello ' + name + '. Your age is ' + age + '.');
   }
*/


/*// Your code here!
// Your code here!
function getAgeStatus(age) {
    switch (age) {
        case 18:
            console.log("You are not allowed to party.");
            break;
        case 21:
            console.log("You are now part of the adult society.");
            break;
        case 65:
            console.log("We thank you for your contribution to society.");
            break;
        default:
            console.log("Are you sure you're not an alien?");
            break;
    }
}
}*/

/*function isLegalAge(age) {
    age >= 18 ? console.log("You are of legal age.") : console.log("You are not allowed here.");
}*/

/*// Do not modify!
function isLegalAge(age) {
    age >= 18 ? console.log("You are of legal age.") : console.log("You are not allowed here.");
}




function safeIsLegalAge(age) {
    // Your code here!
    try {
        isLegalag(age);
    } catch (error) {
        console.log("Did catch an error");
    } finally {
        isLegalAge(age);
    }
}*/


/*// Do not modify!
function isLegalAge(age) {
    age >= 18 ? console.log("You are of legal age.") : console.log("You are not allowed here.");
}




function safeIsLegalAge(age) {
    // Your code here!
    try {
        isLegalag(age);
    } catch (error) {
        console.log("Did catch an error");
    } finally {
        isLegalAge(age);
    }
}*/